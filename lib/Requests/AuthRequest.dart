import 'dart:convert';
import 'package:http/http.dart' as http;
import 'MainApiRout.dart';

class ThreadAuth {
  final String route;
  final String email;
  final String password;
  final String username;


  ThreadAuth(this.route, this.email, this.password, this.username);

  Future<http.Response> registerUser() {
    return http.post(
      Uri.http(MainApiRout.mainRout, route),
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
      body: jsonEncode(<String, String>{
        'email': email,
        'password': password,
        'username': username,

      }),
    );
  }
}
