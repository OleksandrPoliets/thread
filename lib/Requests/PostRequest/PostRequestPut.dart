import 'dart:convert';
import 'package:http/http.dart' as http;
import '../MainApiRout.dart';

class PostRequestPut {
  final String route;
  final String token;
  final Map<String, String> postId;

  PostRequestPut(this.route, this.token, this.postId);

  Future<http.Response> putComment() {
    return http.post(
      Uri.http(MainApiRout.mainRout, route),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      },
      body: jsonEncode(postId),
    );
  }
}
