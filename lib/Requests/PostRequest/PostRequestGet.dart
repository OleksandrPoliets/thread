import 'package:http/http.dart' as http;

import '../MainApiRout.dart';

class PostRequestGet {
  final String route;
  final String token;

  PostRequestGet(this.route, this.token);

  Future<http.Response> getPosts() {
    return http.get(
      Uri.http(MainApiRout.mainRout, route),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      });
  }
}