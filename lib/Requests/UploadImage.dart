import 'package:http/http.dart' as http;
import 'MainApiRout.dart';


class PhotoUpload {
  final String token;
  final String route;
  final String photoRout;


  PhotoUpload(this.token, this.route, this.photoRout);


  Future<String> upload() async{
    var request = http.MultipartRequest('POST', Uri.http(MainApiRout.mainRout, route));
    request.files.add(await http.MultipartFile.fromPath('image', photoRout));
    request.headers['Authorization'] = 'Bearer $token';
    var res = await request.send();
    var result = await http.Response.fromStream(res);

    return result.body;
  }

}