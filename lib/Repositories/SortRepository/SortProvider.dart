import 'package:flutter/foundation.dart';

class SortProvider with ChangeNotifier {
 int _value = 1;

  int getVaValue(){
    return _value;
  }

  void setValue(int value) {
    _value = value;
    notifyListeners();
  }

}