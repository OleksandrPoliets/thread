import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:tread_app/Requests/PostRequest/PostRequestGet.dart';
import 'package:tread_app/Requests/PostRequest/PostRequestPut.dart';
import 'package:tread_app/Requests/UploadImage.dart';
import 'dart:io';
import '../RoutsToApi.dart';
import 'PostModel.dart';

class PostProvider with ChangeNotifier {
  List<Post> post = [];
  List<Post> filters = [];

  String errorMessage;
  bool loading = false;
  bool isError = false;

  List<Post> parsePost(String data) {
    final parsed = jsonDecode(data).cast<Map<String, dynamic>>();

    return parsed.map<Post>((json) => Post.fromJson(json)).toList();
  }

  void getPostData(String token, String rout) async {
    setLoading(false);
    await PostRequestGet(rout, token).getPosts().then((data) {

      if (data.statusCode == 200) {
        setPost(parsePost(data.body));
      } else {
        Map<String, dynamic> result = json.decode(data.body);
        print(result['message']);
        setError(true);
      }
    });
  }

  void createNewPOst(String token, String body, File photo) async {
    setLoading(false);
    Map<String, dynamic> result;
    Map<String, String> sendBody = {
      'body': body,
    };

    if(photo != null){
      String loadedPhoto = await PhotoUpload(token, RoutsToApi.routToImage, photo.path).upload();
      final parsed = jsonDecode(loadedPhoto);
      sendBody['imageId'] = parsed['id'];
    }

    await PostRequestPut(RoutsToApi.ollPosts, token, sendBody).putComment().then((data) {
      if (data.statusCode == 200) {
        result = json.decode(data.body);
      } else {
        Map<String, dynamic> result = json.decode(data.body);
        print(result['message']);
        setError(false);
      }
    });
    await PostRequestGet('${RoutsToApi.ollPosts}/${result['id']}', token).getPosts().then((data) {
      if (data.statusCode == 200) {
        addNewPost(Post.fromJson(json.decode(data.body)));

      } else {
        Map<String, dynamic> result = json.decode(data.body);
        print(result['message']);
        setError(false);
      }
    });
  }

  int findPostIndex(String id, List<Post> postToFindIndex) => postToFindIndex.indexWhere((postItem) => postItem.id == id);

  void addNewPost(Post value) {
    post.insert(0, value);
    filters.insert(0, value);
    setLoading(true);
  }

  void deletePost(String id){
    int postIndex = findPostIndex(id, post);
    int filtersIndex = findPostIndex(id, filters);
    setLoading(false);
    post.removeAt(postIndex);
    filters.removeAt(filtersIndex);
    setLoading(true);
  }


  void editPost(String id, String newBody){
    int postIndex = findPostIndex(id, post);
    int filtersIndex = findPostIndex(id, filters);
    var now = DateTime.now();
    setLoading(false);
    post[postIndex].body = newBody;
    post[postIndex].updatedAt = now.toUtc().toString();
    filters[filtersIndex].body = newBody;
    filters[filtersIndex].updatedAt = now.toUtc().toString();
    setLoading(true);
  }

  void setCommentCount (String id, int addCommentCount){
    post[findPostIndex(id, post)].commentCount += addCommentCount;
    notifyListeners();
  }

  void postReaction(String postId, bool isLike){
    int indexInPost = findPostIndex(postId, post);
    if(isLike){
      if(post[indexInPost].isLike){
        post[indexInPost].isLike = false;
        post[indexInPost].likeCount -= 1;
      } else{
        post[indexInPost].isLike = true;
        post[indexInPost].likeCount += 1;
      }
      if(post[indexInPost].isDislike){
        post[indexInPost].isDislike = false;
        post[indexInPost].dislikeCount -= 1;
      }
    } else{
      if(post[indexInPost].isDislike){
        post[indexInPost].isDislike = false;
        post[indexInPost].dislikeCount -= 1;
      } else{
        post[indexInPost].isDislike = true;
        post[indexInPost].dislikeCount += 1;
      }
      if(post[indexInPost].isLike){
        post[indexInPost].isLike = false;
        post[indexInPost].likeCount -= 1;
      }
    }
    notifyListeners();
  }

  void setLoading(bool value) {
    loading = value;
    notifyListeners();
  }

  bool isLoading() {
    return loading;
  }

  void setPost(List<Post> value) {
    post.clear();
    filters.clear();

    post.addAll(value);
    filters.addAll(value);

    setLoading(true);
  }

  void showOllPosts(){
    setLoading(false);
    filters.clear();
    filters.addAll(post);
    setLoading(true);
  }

  void sortByUser(String id){
    setLoading(false);
    filters.clear();
    filters = post.where((user) => (user.userId.contains(id))).toList();
    setLoading(true);
  }

  void sortByDate(bool asc){
    setLoading(false);
    filters.clear();
    filters.addAll(post);
    filters.sort((Post a, Post b) {
      if(asc) {
        return a.createdAt.compareTo(b.createdAt);
      } else {
        return b.createdAt.compareTo(a.createdAt);
      }
    });
    setLoading(true);
  }

  void sortByLikeCount(bool asc){
    setLoading(false);
    filters.clear();
    filters.addAll(post);
    filters.sort((Post a, Post b) {
      if(asc) {
        return a.likeCount.compareTo(b.likeCount);
      } else {
        return b.likeCount.compareTo(a.likeCount);
      }
    });
    setLoading(true);
  }

  void sortByDislikeCount(bool asc){
    setLoading(false);
    filters.clear();
    filters.addAll(post);
    filters.sort((Post a, Post b) {
      if(asc) {
        return a.dislikeCount.compareTo(b.dislikeCount);
      } else {
        return b.dislikeCount.compareTo(a.dislikeCount);
      }
    });
    setLoading(true);
  }

  void setMessage(String value) {
    errorMessage = value;
    notifyListeners();
  }

  void setError(bool value) {
    isError = value;
    notifyListeners();
  }

  String getMessage() {
    return errorMessage;
  }

}