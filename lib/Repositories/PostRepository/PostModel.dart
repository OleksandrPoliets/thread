class Post {
  final String id;
  final String userId;
  final String image;
  String body;
  int  commentCount;
  final String createdAt;
  String updatedAt;
  final String postOwner;
  final String postOwnerImg;
  bool isLike;
  bool isDislike;
  int dislikeCount;
  int likeCount;

  Post(
      {this.id,
      this.userId,
      this.image,
      this.body,
      this.commentCount,
      this.dislikeCount,
      this.likeCount,
      this.createdAt,
      this.updatedAt,
      this.postOwner,
      this.postOwnerImg,
      this.isLike,
      this.isDislike
      });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      id: json['id'],
      userId: json['userId'],
      image: json['image'] == null ? null : json['image']['link'],
      body: json['body'],
      commentCount: int.parse(json['commentCount']),
      dislikeCount: int.parse(json['dislikeCount']),
      likeCount: int.parse(json['likeCount']),
      createdAt: json['createdAt'],
      updatedAt: json['updatedAt'],
      postOwner: json['user']['username'],
      postOwnerImg: json['user']['image'] == null ? null : json['user']['image']['link'],
      isLike: false,
      isDislike: false,
    );
  }
}
