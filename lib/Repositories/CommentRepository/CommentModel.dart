class PostComment {
  final String id;
  String body;
  final String postId;
  final String createdAt;
  String updatedAt;
  final String userId;
  final String commentOwnerImg;
  final String commentOwnerName;
  bool isLike;
  bool isDislike;
  int dislikeCount;
  int likeCount;


  PostComment(
      {
        this.id,
        this.userId,
        this.body,
        this.postId,
        this.dislikeCount,
        this.likeCount,
        this.createdAt,
        this.updatedAt,
        this.commentOwnerImg,
        this.commentOwnerName,
        this.isLike,
        this.isDislike
      });

  factory PostComment.fromJson(Map<String, dynamic> json) {
    return PostComment(
      id: json['id'],
      postId: json['postId'],
      body: json['body'],
      createdAt: json['createdAt'],
      updatedAt: json['updatedAt'],
      userId: json['user']['id'],
      commentOwnerName: json['user']['username'],
      commentOwnerImg: json['user']['image'] == null ? null : json['user']['image']['link'],
      isLike: false,
      isDislike: false,
      likeCount: 0,
      dislikeCount: 0
    );
  }
}