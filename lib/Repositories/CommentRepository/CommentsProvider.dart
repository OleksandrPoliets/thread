import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tread_app/Requests/PostRequest/PostRequestGet.dart';
import 'package:tread_app/Requests/PostRequest/PostRequestPut.dart';
import '../RoutsToApi.dart';
import 'CommentModel.dart';

class CommentsProvider with ChangeNotifier {
  List<String> _loadedPostComments = [];
  List<PostComment> comments = [];
  List<PostComment> filteredComments = [];
  String errorMessage;
  bool loading = true;
  bool isError = false;

  List<PostComment> parseComments(String data) {
    List<PostComment> tempComents = [];
    Map<String, dynamic> result = json.decode(data);

    if(result['comments'].isNotEmpty){
      for (var comment in result['comments']) {
        tempComents.add(PostComment.fromJson(comment));
      }
    }

    return tempComents;
  }

  void getPostData(String token, String id) async {
    setLoading(true);
    await PostRequestGet('${RoutsToApi.ollPosts}/$id', token).getPosts().then((data) {

      if (data.statusCode == 200) {
        setComments(parseComments(data.body));
      } else {
        Map<String, dynamic> result = json.decode(data.body);
        print(result['message']);
        setError(true);
      }
    });
  }

  Future<bool> postComment(String token, String body, String postId) async {
    setLoading(true);
    Map<String, dynamic> result;
    bool isOk = false;
    Map<String, String> sendBody = {
      'body': body,
      'postId': postId
    };

    await PostRequestPut(RoutsToApi.routToComment, token, sendBody).putComment().then((data) {
      if (data.statusCode == 200) {
        result = json.decode(data.body);
      } else {
        Map<String, dynamic> result = json.decode(data.body);
        print(result['message']);
        setError(true);
      }
    });
    await PostRequestGet('${RoutsToApi.routToComment}/${result['id']}', token).getPosts().then((data) {
      if (data.statusCode == 200) {
        addNewComment(PostComment.fromJson(json.decode(data.body)));
        isOk = true;
      } else {
        Map<String, String> result = json.decode(data.body);
        print(result['message']);
        setError(true);
      }
    });
    return isOk;
  }

  int findCommentIndex(String id, List<PostComment> listToFind) => listToFind.indexWhere((commentItem) => commentItem.id == id);

  void deleteComment(String id){
    int commentsIndex = findCommentIndex(id, comments);
    int filteredCommentsIndex = findCommentIndex(id, filteredComments);
    setLoading(true);

    comments.removeAt(commentsIndex);
    filteredComments.removeAt(filteredCommentsIndex);

    setLoading(false);
  }

  void selectCommentsToShow(String postId){
      filteredComments.clear();
      filteredComments = comments.where((post) => (post.postId.contains(postId))).toList();
  }

  void loadCommentList(String userToken, String postId){
    bool isLoaded = _loadedPostComments.contains(postId);
    setLoading(true);
    if(isLoaded){
      selectCommentsToShow(postId);
      setLoading(false);
    } else {
      _loadedPostComments.add(postId);
      getPostData(userToken, postId);
    }
  }

  void editComment(String id, String newBody){
    int commentsIndex = findCommentIndex(id, comments);
    var now = DateTime.now();
    setLoading(true);

    comments[commentsIndex].body = newBody;
    comments[commentsIndex].updatedAt = now.toUtc().toString();
    selectCommentsToShow(comments[commentsIndex].postId);

    setLoading(false);
  }

  void setLoading(bool value) {
    loading = value;
    notifyListeners();
  }

  bool isLoading() {
    return loading;
  }

  void addNewComment(PostComment value) {
    comments.insert(0, value);
    filteredComments.insert(0, value);
    setLoading(false);
  }

  void setComments(List<PostComment> value) {
    filteredComments.clear();
    comments.addAll(value);
    filteredComments.addAll(value);
    setLoading(false);
  }

  void setMessage(String value) {
    errorMessage = value;
    notifyListeners();
  }

  void setError(bool value) {
    isError = value;
    notifyListeners();
  }

  String getMessage() {
    return errorMessage;
  }

  int findComment(String id, List<PostComment> listComments) => listComments.indexWhere((comment) => comment.id == id);

  void commentReaction(String postId, bool isLike){
    int indexInPost = findComment(postId, comments);
    if(isLike){
      if(comments[indexInPost].isLike){
        comments[indexInPost].isLike = false;
        comments[indexInPost].likeCount -= 1;
      } else{
        comments[indexInPost].isLike = true;
        comments[indexInPost].likeCount += 1;
      }
      if(comments[indexInPost].isDislike){
        comments[indexInPost].isDislike = false;
        comments[indexInPost].dislikeCount -= 1;
      }
    } else{
      if(comments[indexInPost].isDislike){
        comments[indexInPost].isDislike = false;
        comments[indexInPost].dislikeCount -= 1;
      } else{
        comments[indexInPost].isDislike = true;
        comments[indexInPost].dislikeCount += 1;
      }
      if(comments[indexInPost].isLike){
        comments[indexInPost].isLike = false;
        comments[indexInPost].likeCount -= 1;
      }
    }
    notifyListeners();
  }

}