import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:tread_app/Requests/AuthRequest.dart';
import 'UserModel.dart';

class UserProvider with ChangeNotifier {
  User user;
  List post;
  String errorMessage;
  bool loading = false;
  bool isError = false;

  Future<bool> registerUser(String rout, String email, String password, [String username = '']) async {
    setLoading(true);

    await ThreadAuth(rout, email, password, username).registerUser().then((data) {
      setLoading(false);

      if (data.statusCode == 200) {
        setUser(User.fromJson(json.decode(data.body)));
      } else {
        Map<String, dynamic> result = json.decode(data.body);
        setMessage(result['message']);
        setError(true);
      }
    });

    return isUser();
  }

  void setLoading(bool value) {
    loading = value;
    notifyListeners();
  }

  bool isLoading() {
    return loading;
  }

  void setUser(User value) {
    user = value;
    notifyListeners();
  }

  void logOut() {
    user = null;
  }

  User getUSer() {
    return user;
  }

  void setMessage(String value) {
    errorMessage = value;
    notifyListeners();
  }

  void setError(bool value) {
    isError = value;
    notifyListeners();
  }

  String getMessage() {
    return errorMessage;
  }

  bool isUser() {
    return user != null ? true : false;
  }
}