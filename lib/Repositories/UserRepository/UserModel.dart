class User {
  final String id;
  final String token;
  final String email;
  final String username;
  final String image;
  User({this.id, this.token, this.email, this.username, this.image});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        token: json['token'],
        id: json['user']['id'],
        email: json['user']['email'],
        username: json['user']['username'],
        image: json['user']['image'] == null ? null : json['user']['image']['link'],
    );
  }
}