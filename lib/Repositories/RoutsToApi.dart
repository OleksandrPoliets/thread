class RoutsToApi {
  static final String login = 'api/auth/login';
  static final String register = 'api/auth/register';
  static final String ollPosts = 'api/posts';
  static final String postReaction = 'api/posts/react';
  static final String routToComment = 'api/comments';
  static final String routToImage = 'api/images';
}