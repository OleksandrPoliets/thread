import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tread_app/Repositories/UserRepository/UserProvider.dart';

class ErrorMessage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => AlertDialog(
    title: Text('Error'),
    content: Text(Provider.of<UserProvider>(context, listen: false).getMessage()),
    actions: [
      FlatButton(
        textColor: Color(0xFF6200EE),
        onPressed: () {
          Provider.of<UserProvider>(context, listen: false).setError(false);
        },
        child: Text('Ok'),
      ),
    ],
  );
}