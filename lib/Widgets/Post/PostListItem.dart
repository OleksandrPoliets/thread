import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tread_app/Repositories/CommentRepository/CommentsProvider.dart';
import 'package:tread_app/Repositories/PostRepository/PostProvider.dart';
import 'package:tread_app/Repositories/UserRepository/UserProvider.dart';
import 'package:tread_app/Widgets/ContainerComponent/HeaderCardContainer.dart';
import 'package:tread_app/Widgets/PostCommentForm/FormTitles.dart';
import '../PostCommentForm/HeroForm.dart';
import '../ContainerComponent/TextBodyContainer.dart';
import 'PostDetails.dart';

class PostListItem extends StatelessWidget {
  final int index;
  final bool fromMain;

  const PostListItem({Key key, this.index, this.fromMain}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final post = Provider.of<PostProvider>(context).filters[index];
    final userId = Provider.of<UserProvider>(context).user.id;
    return Card(
      margin: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0, bottom: 15.0),
      elevation: 3,
      child: InkWell(
        onTap: () {
          if (fromMain) {
            Provider.of<CommentsProvider>(context, listen: false)
                .loadCommentList(
                    Provider.of<UserProvider>(context, listen: false)
                        .user
                        .token,
                    post.id);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PostDetails(postIndex: index)));
          }
        },
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            HeaderCardContainer(
                postOwner: post.postOwner, createdAt: post.createdAt),
            post.image == null
                ? SizedBox(
                    height: 0,
                  )
                : Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(10.0),
                    child: Image.network(post.image, fit: BoxFit.contain),
                  ),
            TextBodyContainer(body: post.body),
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(
                    color: Colors.grey[300],
                    width: 1.0,
                  ),
                ),
              ),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Row(
                      children: [
                        IconButton(
                          icon: Icon(Icons.thumb_up),
                          onPressed: () {
                            Provider.of<PostProvider>(context, listen: false)
                                .postReaction(post.id, true);
                          },
                          color: post.isLike ? Colors.green : Colors.grey,
                        ),
                        Text(post.likeCount.toString()),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Row(
                      children: [
                        IconButton(
                            icon: Icon(Icons.thumb_down),
                            color:
                                post.isDislike ? Colors.redAccent : Colors.grey,
                            onPressed: () {
                              Provider.of<PostProvider>(context, listen: false)
                                  .postReaction(post.id, false);
                            }),
                        Container(
                          child: Text(post.dislikeCount.toString()),
                        ),
                      ],
                    ),
                  ),
                  fromMain
                      ? Container(
                          margin: EdgeInsets.only(right: 10.0),
                          child: Row(
                            children: [
                              Icon(
                                Icons.comment,
                                color: Colors.grey,
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 5.0),
                                child: Text(post.commentCount.toString()),
                              ),
                            ],
                          ),
                        )
                      : SizedBox(
                          height: 0,
                        ),
                  post.userId == userId && fromMain
                      ? Container(
                          child: Row(
                            children: [
                              FlatButton.icon(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => HeroForm(
                                                title: FormTitles.editPost,
                                                text: post.body,
                                                id: post.id,
                                              )));
                                },
                                icon: Icon(
                                  Icons.create,
                                  color: Colors.grey,
                                ),
                                label: Text(
                                  'Edit',
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ),
                              FlatButton.icon(
                                onPressed: () {
                                  Provider.of<PostProvider>(context,
                                          listen: false)
                                      .deletePost(post.id);
                                },
                                icon: Icon(
                                  Icons.delete_forever,
                                  color: Colors.grey,
                                ),
                                label: Text(
                                  'Delete',
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ),
                            ],
                          ),
                        )
                      : SizedBox(
                          height: 0,
                          width: 0,
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
