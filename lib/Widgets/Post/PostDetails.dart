import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tread_app/Repositories/CommentRepository/CommentsProvider.dart';
import 'package:tread_app/Repositories/PostRepository/PostProvider.dart';
import 'package:tread_app/Widgets/PostCommentForm/FormTitles.dart';
import '../Comments/CommentListItem.dart';
import '../PostCommentForm/HeroForm.dart';
import 'PostListItem.dart';

class PostDetails extends StatelessWidget {
  final int postIndex;

  const PostDetails({Key key, this.postIndex}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Post details'),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 30.0,
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: 'HeroForm',
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      HeroForm(
                        title: FormTitles.addComment,
                        text: '',
                        id: Provider.of<PostProvider>(context, listen: false).post[postIndex].id,
                      )
              )
          );
        },
        icon: Icon(Icons.add_comment),
        label: Text("Add comment"),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
              [
                PostListItem(index: postIndex, fromMain: false),
                Container(
                  margin: EdgeInsets.only(top: 20.0, right: 10.0, left: 10.0),
                  padding: EdgeInsets.only(top: 5.0),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Colors.grey[300],
                        width: 1.0,
                      ),
                    ),
                  ),
                  child: Text(
                    'Comments',
                    style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.black,
                    ),
                  ),
                ),
                Provider.of<CommentsProvider>(context)
                        .filteredComments
                        .isEmpty
                    ? Container(
                        margin: EdgeInsets.only(
                            top: 20.0, right: 10.0, left: 10.0, bottom: 20.0),
                        child: Text('No Comments'),
                      )
                    : SizedBox(
                        height: 0,
                      ),
              ],
            ),
          ),
          Provider.of<CommentsProvider>(context).isLoading()
              ? SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      if (index > 0) return null;
                      return Center(child: CircularProgressIndicator());
                    },
                  ),
                )
              : SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      if (index >
                          Provider.of<CommentsProvider>(context, listen: false)
                                  .filteredComments
                                  .length -
                              1) return null;
                      return CommentListItem(
                          comment: Provider.of<CommentsProvider>(context,
                                  listen: false)
                              .filteredComments[index]);
                    },
                  ),
                ),
        ],
      ),
    );
  }
}