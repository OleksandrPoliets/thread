import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tread_app/Repositories/CommentRepository/CommentsProvider.dart';
import 'package:tread_app/Repositories/PostRepository/PostProvider.dart';
import 'package:tread_app/Repositories/UserRepository/UserProvider.dart';
import 'package:tread_app/Widgets/ContainerComponent/HeaderCardContainer.dart';
import 'package:tread_app/Widgets/PostCommentForm/FormTitles.dart';
import '../PostCommentForm/HeroForm.dart';
import '../ContainerComponent/TextBodyContainer.dart';
import '../userAvatar/UserAvatar.dart';

class CommentListItem extends StatelessWidget {
  final comment;

  const CommentListItem({Key key, this.comment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String userId =
        Provider.of<UserProvider>(context, listen: false).user.id;
    return Container(
      margin: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            children: [
              UserAvatar(
                avatar: comment.commentOwnerImg,
              ),
              Flexible(
                flex: 1,
                child: Container(
                  child: Column(
                    children: [
                      HeaderCardContainer(
                          postOwner: comment.commentOwnerName,
                          createdAt: comment.createdAt),
                      TextBodyContainer(body: comment.body),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10.0, top: 10.0),
            padding: EdgeInsets.only(top: 10.0, left: 10.0, bottom: 10.0),
            width: double.infinity,
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  color: Colors.grey[300],
                  width: 1.0,
                ),
                bottom: BorderSide(
                  color: Colors.grey[300],
                  width: 1.0,
                ),
              ),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10.0),
                        child: Row(
                          children: [
                            IconButton(
                                icon: Icon(Icons.thumb_up),
                                color:
                                    comment.isLike ? Colors.green : Colors.grey,
                                onPressed: () {
                                  Provider.of<CommentsProvider>(context,
                                          listen: false)
                                      .commentReaction(comment.id, true);
                                }),
                            Container(
                              margin: EdgeInsets.only(left: 5.0),
                              child: Text(comment.likeCount.toString()),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 10.0),
                        child: Row(
                          children: [
                            IconButton(
                                icon: Icon(Icons.thumb_down),
                                color: comment.isDislike
                                    ? Colors.redAccent
                                    : Colors.grey,
                                onPressed: () {
                                  Provider.of<CommentsProvider>(context,
                                          listen: false)
                                      .commentReaction(comment.id, false);
                                }),
                            Container(
                              margin: EdgeInsets.only(left: 5.0),
                              child: Text(comment.dislikeCount.toString()),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                comment.userId == userId
                    ? Container(
                        child: Row(
                          children: [
                            FlatButton.icon(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => HeroForm(
                                              title: FormTitles.editComment,
                                              text: comment.body,
                                              id: comment.id,
                                            )));
                              },
                              icon: Icon(
                                Icons.create,
                                color: Colors.grey,
                              ),
                              label: Text(
                                'Edit',
                                style: TextStyle(color: Colors.grey),
                              ),
                            ),
                            FlatButton.icon(
                              onPressed: () {
                                Provider.of<CommentsProvider>(context,
                                        listen: false)
                                    .deleteComment(comment.id);
                                Provider.of<PostProvider>(context,
                                        listen: false)
                                    .setCommentCount(comment.postId, -1);
                              },
                              icon: Icon(Icons.delete_forever,
                                  color: Colors.grey),
                              label: Text(
                                'Delete',
                                style: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ],
                        ),
                      )
                    : SizedBox(
                        height: 0,
                        width: 0,
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
