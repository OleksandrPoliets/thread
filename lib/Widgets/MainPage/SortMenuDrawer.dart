import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tread_app/Repositories/PostRepository/PostProvider.dart';
import 'package:tread_app/Repositories/SortRepository/SortProvider.dart';
import 'package:tread_app/Repositories/UserRepository/UserProvider.dart';


class SortMenuDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          ListTile(
            title: Text(
              'Show all posts',
              style: TextStyle(
                  color: Provider.of<SortProvider>(context, listen: false).getVaValue() == 1 ? Color(0xFF2FA6F1) : Colors.black
              ),
            ),
            leading: Radio(
              value: 1,
              groupValue: Provider.of<SortProvider>(context, listen: false).getVaValue(),
              activeColor: Color(0xFF2FA6F1),
              onChanged: (int value) {
                Provider.of<SortProvider>(context, listen: false).setValue(value);
                Provider.of<PostProvider>(context, listen: false).showOllPosts();
              },
            ),
          ),
          ListTile(
            title: Text(
              'Show only my posts',
              style: TextStyle(
                  color: Provider.of<SortProvider>(context, listen: false).getVaValue() == 2 ? Color(0xFF2FA6F1) : Colors.black
              ),
            ),
            leading: Radio(
              value: 2,
              groupValue: Provider.of<SortProvider>(context, listen: false).getVaValue(),
              activeColor: Color(0xFF2FA6F1),
              onChanged: (int value) {
                Provider.of<SortProvider>(context, listen: false).setValue(value);
                Provider.of<PostProvider>(context, listen: false).sortByUser(
                    Provider.of<UserProvider>(context, listen: false).user.id
                );
              },
            ),
          ),
          ListTile(
            title: Text(
              'Sort by date (asc)',
              style: TextStyle(
                  color: Provider.of<SortProvider>(context, listen: false).getVaValue() == 3 ? Color(0xFF2FA6F1) : Colors.black
              ),
            ),
            leading: Radio(
              value: 3,
              groupValue: Provider.of<SortProvider>(context, listen: false).getVaValue(),
              activeColor: Color(0xFF2FA6F1),
              onChanged: (int value) {
                Provider.of<SortProvider>(context, listen: false).setValue(value);
                Provider.of<PostProvider>(context, listen: false).sortByDate(true);
              },
            ),
          ),
          ListTile(
            title: Text(
              'Sort by date (desc)',
              style: TextStyle(
                  color: Provider.of<SortProvider>(context, listen: false).getVaValue() == 4 ? Color(0xFF2FA6F1) : Colors.black
              ),
            ),
            leading: Radio(
              value: 4,
              groupValue: Provider.of<SortProvider>(context, listen: false).getVaValue(),
              activeColor: Color(0xFF2FA6F1),
              onChanged: (int value) {
                Provider.of<SortProvider>(context, listen: false).setValue(value);
                Provider.of<PostProvider>(context, listen: false).sortByDate(false);
              },
            ),
          ),
          ListTile(
            title: Text(
              'Sort by like (asc)',
              style: TextStyle(
                  color: Provider.of<SortProvider>(context, listen: false).getVaValue() == 5 ? Color(0xFF2FA6F1) : Colors.black
              ),
            ),
            leading: Radio(
              value: 5,
              groupValue: Provider.of<SortProvider>(context, listen: false).getVaValue(),
              activeColor: Color(0xFF2FA6F1),
              onChanged: (int value) {
                Provider.of<SortProvider>(context, listen: false).setValue(value);
                Provider.of<PostProvider>(context, listen: false).sortByLikeCount(true);
              },
            ),
          ),
          ListTile(
            title: Text(
              'Sort by like (desc)',
              style: TextStyle(
                  color: Provider.of<SortProvider>(context, listen: false).getVaValue() == 6 ? Color(0xFF2FA6F1) : Colors.black
              ),
            ),
            leading: Radio(
              value: 6,
              groupValue: Provider.of<SortProvider>(context, listen: false).getVaValue(),
              activeColor: Color(0xFF2FA6F1),
              onChanged: (int value) {
                Provider.of<SortProvider>(context, listen: false).setValue(value);
                Provider.of<PostProvider>(context, listen: false).sortByLikeCount(false);
              },
            ),
          ),
          ListTile(
            title: Text(
              'Sort by dislike (asc)',
              style: TextStyle(
                  color: Provider.of<SortProvider>(context, listen: false).getVaValue() == 7 ? Color(0xFF2FA6F1) : Colors.black
              ),
            ),
            leading: Radio(
              value: 7,
              groupValue: Provider.of<SortProvider>(context, listen: false).getVaValue(),
              activeColor: Color(0xFF2FA6F1),
              onChanged: (int value) {
                Provider.of<SortProvider>(context, listen: false).setValue(value);
                Provider.of<PostProvider>(context, listen: false).sortByDislikeCount(true);
              },
            ),
          ),
          ListTile(
            title: Text(
              'Sort by dislike (desc)',
              style: TextStyle(
                  color: Provider.of<SortProvider>(context, listen: false).getVaValue() == 8 ? Color(0xFF2FA6F1) : Colors.black
              ),
            ),
            leading: Radio(
              value: 8,
              groupValue: Provider.of<SortProvider>(context, listen: false).getVaValue(),
              activeColor: Color(0xFF2FA6F1),
              onChanged: (int value) {
                Provider.of<SortProvider>(context, listen: false).setValue(value);
                Provider.of<PostProvider>(context, listen: false).sortByDislikeCount(false);
              },
            ),
          ),
        ],
      ),
    );
  }
}

