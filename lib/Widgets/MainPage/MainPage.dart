import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tread_app/Repositories/PostRepository/PostProvider.dart';
import 'package:tread_app/Repositories/UserRepository/UserProvider.dart';
import 'package:tread_app/Widgets/PostCommentForm/FormTitles.dart';
import 'package:tread_app/Widgets/userAvatar/UserAvatar.dart';
import '../PostCommentForm/FabPostCommentForm.dart';
import '../Post/PostListItem.dart';
import 'SortMenuDrawer.dart';
import 'UserMenuDrawer.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (context) {
            return InkWell(
                    onTap: () {
                      Scaffold.of(context).openDrawer();
                    },
                    child: UserAvatar(
                      avatar: Provider.of<UserProvider>(context, listen: false).user.image,
                      containerWidth: 80,
                      containerHeight: 80,
                    ),
                  );
          },
        ),
        title: Text(
            Provider.of<UserProvider>(context, listen: false).user.username),
        actions: <Widget>[
          Builder(
            builder: (context) {
              return IconButton(
                icon: Icon(Icons.sort, size: 30),
                onPressed: () {
                  Scaffold.of(context).openEndDrawer();
                },
              );
            },
          )
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 30.0,
        ),
      ),
      floatingActionButton: FabPostCommentForm(FabLable: FormTitles.addPost, title: FormTitles.addPost,),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      drawer: UserMenuDrawer(),
      endDrawer: SortMenuDrawer(),
      body: Column(
        children: [
          Provider.of<PostProvider>(context).loading &&
                  Provider.of<PostProvider>(context).filters.isNotEmpty
              ? Expanded(
                  child: ListView.builder(
                      itemCount:
                          Provider.of<PostProvider>(context, listen: false)
                              .filters
                              .length,
                      itemBuilder: (BuildContext context, int index) {
                        return PostListItem(
                            index: index,
                            fromMain: true);
                      }),
                )
              : Expanded(
                  child: Provider.of<PostProvider>(context).loading
                      ? Center(
                          child: Text('No posts'),
                        )
                      : Center(child: CircularProgressIndicator()),
                ),
        ],
      ),
    );
  }
}
