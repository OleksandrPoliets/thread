import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tread_app/Repositories/UserRepository/UserProvider.dart';
import 'package:tread_app/Widgets/userAvatar/UserAvatar.dart';

import '../PagesRoutes.dart';

class UserMenuDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.grey,
            ),
            child: Center(
              child: Column(
                children: <Widget>[
                  UserAvatar(
                    avatar: Provider.of<UserProvider>(context, listen: false)
                        .user
                        .image,
                    containerWidth: 100,
                    containerHeight: 100,
                  ),
                  Text(Provider.of<UserProvider>(context, listen: false)
                      .user
                      .username),
                ],
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('Profile'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, PagesRoutes.profile);
            },
          ),
          ListTile(
            leading: Icon(Icons.logout),
            title: Text('Log out'),
            onTap: () {
              Navigator.pop(context);
              Provider.of<UserProvider>(context, listen: false).logOut();
              Navigator.pushNamed(context, PagesRoutes.login);
            },
          ),
        ],
      ),
    );
  }
}
