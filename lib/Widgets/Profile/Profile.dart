import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tread_app/Repositories/UserRepository/UserProvider.dart';
import 'package:tread_app/Widgets/userAvatar/UserAvatar.dart';

class UserProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        title: Text('Profile'),
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          color: Colors.grey[100],
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                UserAvatar(
                  avatar: Provider.of<UserProvider>(context, listen: false).user.image,
                  containerWidth: 140,
                  containerHeight: 140,
                ),
                Card(
                  margin: EdgeInsets.only(top: 30.0, bottom: 30.0),
                  child: Container(
                    margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 10.0),
                          child: Icon(Icons.person),
                        ),
                        Text(
                          Provider.of<UserProvider>(context, listen: false).user.username,
                          style: TextStyle(
                            color: Colors.black, fontSize: 30, ),
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  margin: EdgeInsets.only(bottom: 30.0),
                  child: Container(
                    margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 10.0),
                          child: Icon(Icons.email_outlined),
                        ),
                        Text(
                          Provider.of<UserProvider>(context, listen: false).user.email,
                          style: TextStyle(
                            color: Colors.black, fontSize: 30, ),
                        ),
                      ],
                    ),
                  ),
                ),
                MaterialButton(
                  color: Theme.of(context).accentColor,
                  height: 50.0,
                  minWidth: 300.0,
                  onPressed: () {
                    Provider.of<UserProvider>(context, listen: false).logOut();
                    Navigator.pushNamed(context, '/login');
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10.0),
                        child: Icon(Icons.logout, size: 30),
                      ),
                      Text('Log out'),
                    ],
                  ),
                ),

              ],
            ),
          ),
        ),
    );
  }
}