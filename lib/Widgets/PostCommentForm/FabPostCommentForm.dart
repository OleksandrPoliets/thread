import 'package:flutter/material.dart';

import 'HeroForm.dart';

class FabPostCommentForm extends StatelessWidget {
  final String title;
  final String text;
  final String id;
  final String FabLable;

  const FabPostCommentForm({Key key, this.title, this.text = '', this.id = '', this.FabLable}) : super(key: key);
  @override
  Widget build(BuildContext context) => FloatingActionButton.extended(
    heroTag: 'HeroForm',
    onPressed: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  HeroForm(
                    title: title,
                    text: text,
                    id: id,
                  )
          )
      );
    },
    icon: Icon(Icons.add_comment),
    label: Text(FabLable),
  );
}


