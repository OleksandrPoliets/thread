import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'dart:io';
import 'package:tread_app/Repositories/CommentRepository/CommentsProvider.dart';
import 'package:tread_app/Repositories/PostRepository/PostProvider.dart';
import 'package:tread_app/Repositories/UserRepository/UserProvider.dart';

import 'FormTitles.dart';

class HeroForm extends StatefulWidget {
  final String title;
  final String id;
  final String text;

  HeroForm({Key key, this.title, this.text, this.id}) : super(key: key);

  @override
  _HeroFormState createState() => _HeroFormState();
}

class _HeroFormState extends State<HeroForm> {
  final TextEditingController myController = TextEditingController();
  String _text;

  File _image;
  final picker = ImagePicker();

  Future _imageFromGalery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future _imageFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

    @override
  void initState() {
    super.initState();
    myController.text = widget.text;
    _text = widget.text;
  }
  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: "HeroForm",
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Container(
          padding: EdgeInsets.all(16.0),
          child: Column(
              children: [
                TextField(
                  controller: myController,
                  onChanged: (value){
                    setState(() {
                      _text = value;
                    });
                  },
                  maxLines: 8,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                ),
                widget.title == FormTitles.addPost ? Container(
                  color: Colors.blue,
                  // width: double.infinity,
                  child: MaterialButton(
                    onPressed: (){
                      _showPicker(context);
                    },
                    height: 50.0,
                    child:  Text(
                      "Upload image",
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ) : SizedBox(height: 0,),
                _image != null ? Expanded(
                  child: Image.file(
                    _image,
                    fit: BoxFit.fitHeight,
                  ) ,
                ): SizedBox(
                  height: 0,
                ),
                Container(
                  margin: EdgeInsets.only(top: 15.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        flex: 1,
                        child: Container(
                          color: _text.trim().isNotEmpty ? Colors.greenAccent : Colors.grey,
                          child: MaterialButton(
                            minWidth: 150,
                            onPressed: (){
                              if (_text.trim().isNotEmpty) {
                                _actions();
                                Navigator.pop(context);
                              }
                            },
                            height: 50.0,
                            child:  Text(
                              "Save",
                              style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          color: Colors.redAccent,
                          child: MaterialButton(
                            minWidth: 150,
                            onPressed: (){
                              Navigator.pop(context);
                            },
                            height: 50.0,
                            child:  Text(
                              "Cancel",
                              style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
    );
  }
  void _actions() {
      if(widget.title == FormTitles.addComment){
        Provider.of<CommentsProvider>(context, listen: false).postComment(
            Provider.of<UserProvider>(context, listen: false).user.token,
            _text,
            widget.id
        ).then((value){
          if(value){
            Provider.of<PostProvider>(context, listen: false).setCommentCount(widget.id, 1);
          }
        });
      }
      if(widget.title == FormTitles.addPost){
        Provider.of<PostProvider>(context, listen: false).createNewPOst(
            Provider.of<UserProvider>(context, listen: false).user.token,
            _text,
            _image
        );
      }
      if(widget.title == FormTitles.editComment){
        Provider.of<CommentsProvider>(context, listen: false).editComment(widget.id, _text);
      }
      if(widget.title == FormTitles.editPost){
        Provider.of<PostProvider>(context, listen: false).editPost(widget.id, _text);
      }
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imageFromGalery();
                        Navigator.of(context).pop();
                      }),
                  ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imageFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
}
