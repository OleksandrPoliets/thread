class FormTitles {
  static final String addComment =  'Add comment';
  static final String editComment =  'Edit comment';
  static final String addPost =  'Add post';
  static final String editPost =  'Edit post';
}
