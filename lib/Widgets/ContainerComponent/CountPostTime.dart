String countPostTime(date){
  String result;
  var dateNow = DateTime.parse(date);
  var date2 = DateTime.now();
  int differenceDays = date2.difference(dateNow).inDays;
  int differenceHours = date2.difference(dateNow).inHours;
  int differenceMinutes = date2.difference(dateNow).inMinutes;

  if(differenceDays == 1){
    result = '$differenceDays day ago';
  }

  if(differenceDays > 1){
    result = '$differenceDays days ago';
  }

  if(differenceDays == 0 && differenceHours > 0){
    result = '$differenceHours hours ago';
  }

  if(differenceDays == 0 && differenceHours == 0 && differenceMinutes > 0){
    result = '$differenceMinutes minutes ago';
  }

  if(differenceDays == 0 && differenceHours == 0 && differenceMinutes == 0){
    result = 'a few seconds ago';
  }

  return result;
}