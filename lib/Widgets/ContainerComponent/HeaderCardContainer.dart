import 'package:flutter/material.dart';
import 'CountPostTime.dart';


class HeaderCardContainer extends StatelessWidget {
  final String postOwner;
  final String createdAt;

  const HeaderCardContainer({Key key, this.postOwner, this.createdAt})
      : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        width: double.infinity,
        padding: EdgeInsets.only(top: 10.0, left: 10.0),
        child: Text('Posted by $postOwner - ${countPostTime(createdAt)}'),
      );
}
