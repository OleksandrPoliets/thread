import 'package:flutter/material.dart';

class TextBodyContainer extends StatelessWidget {
  final String body;

  const TextBodyContainer({Key key, this.body}) : super(key: key);
  @override
  Widget build(BuildContext context) => Container(
    width: double.infinity,
    padding: EdgeInsets.all(10.0),
    child: Text(
      body,
      style: TextStyle(
        fontSize: 16.0,
        color: Colors.black,
      ),
    ),
  );
}
