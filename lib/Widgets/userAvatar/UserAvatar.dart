import 'package:flutter/material.dart';
import 'DefaultPhoto.dart';

class UserAvatar extends StatelessWidget {
  final String avatar;
  final double containerWidth;
  final double containerHeight;
  const UserAvatar({Key key, this.avatar, this.containerHeight = 80, this.containerWidth = 80}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: containerWidth,
      height: containerHeight,
      margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
      child: avatar == null ?
      CircleAvatar(backgroundImage: NetworkImage(
          DefaultPhoto.photoRout),
      ) :
      CircleAvatar(backgroundImage: NetworkImage(
          avatar),
      ),
    );
  }
}
