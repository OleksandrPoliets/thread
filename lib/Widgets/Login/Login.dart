import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tread_app/Repositories/UserRepository/UserProvider.dart';
import '../ErrorMessage/ErrorMessage.dart';
import './RegistrationForm.dart';
import 'LoginForm.dart';
import 'LogoWidget.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}


class _LoginState extends State<Login> {
  Color  _loginColor =  Colors.blueAccent;
  Color _registerColor =  Colors.grey;
  bool _loginActive =  true;
  final formKey = new GlobalKey<FormState>();

  void _setLoginActive() {
    setState(() {
      _loginActive = true;
      _loginColor = Colors.blueAccent;
      _registerColor =  Colors.grey;
    });
  }

  void _setRegisterActive() {
    setState(() {
      _loginActive = false;
      _loginColor = Colors.grey;
      _registerColor =  Colors.blueAccent;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          color: Colors.grey[100],
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Logo(),
                ),
                SizedBox(height: 30,),
                Card(
                  margin: const EdgeInsets.only(right: 20.0, left: 20.0, top: 20.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Flexible(
                            flex: 1,
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(width: 1.2, color: _loginColor),
                                  right: BorderSide(width: 1.2, color: Colors.blueAccent),
                                ),
                              ),
                              width: double.infinity,
                              child: MaterialButton(
                                onPressed: _setLoginActive,
                                height: 50.0,
                                child:  Text(
                                  "Sign In",
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    color: _loginColor,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(width: 1.2, color: _registerColor),
                                ),
                              ),
                              width: double.infinity,
                              child: MaterialButton(
                                onPressed: _setRegisterActive,
                                height: 50.0,
                                child:  Text(
                                  "Sign Up",
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    color: _registerColor,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Provider.of<UserProvider>(context).isError
                          ? ErrorMessage()
                          : Container(
                          child: _loginActive ? LoginForm() : RegistrationForm(),
                      ),

                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

