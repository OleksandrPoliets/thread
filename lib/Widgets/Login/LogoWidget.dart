import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 130,
            margin: const EdgeInsets.only(right: 10.0),
            child: Image.asset('assets/images/logo.png'),
          ),
          Text(
            'Thread',
            style: TextStyle(
                color: Colors.black, fontSize: 40, fontWeight: FontWeight.bold),
          ),
        ],
      );
}
