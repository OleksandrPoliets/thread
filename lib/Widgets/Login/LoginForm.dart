import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tread_app/Repositories/PostRepository/PostProvider.dart';
import 'package:tread_app/Repositories/RoutsToApi.dart';
import 'package:tread_app/Repositories/UserRepository/UserProvider.dart';

import '../PagesRoutes.dart';


class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _sizeTextBlack = const TextStyle(fontSize: 20.0, color: Colors.black);
  final _sizeTextWhite = const TextStyle(fontSize: 20.0, color: Colors.white);
  final formKey = new GlobalKey<FormState>();

  String _email;
  String _password;

  void _login() {
    Provider.of<UserProvider>(context, listen: false).setMessage('');
    Provider.of<UserProvider>(context, listen: false)
        .registerUser(RoutsToApi.login, _email, _password)
        .then((value) {
      if (value) {
        Provider.of<PostProvider>(context, listen: false).getPostData(
            Provider.of<UserProvider>(context, listen: false).user.token,
            RoutsToApi.ollPosts
        );
        Navigator.pushNamed(context, PagesRoutes.main);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.only(left: 30.0, right: 30.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Email",
                      prefixIcon: Icon(Icons.email_outlined),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    maxLines: 1,
                    style: _sizeTextBlack,
                    onSaved: (val) => _email = val,
                    validator: (val) =>
                        !val.contains("@") ? 'Not a valid email.' : null,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Password",
                      prefixIcon: Icon(Icons.lock),
                    ),
                    obscureText: true,
                    maxLines: 1,
                    validator: (val) =>
                        val.length < 6 ? 'Password too short.' : null,
                    onSaved: (val) => _password = val,
                    style: _sizeTextBlack,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                  child:
                      Provider.of<UserProvider>(context, listen: false).loading
                          ? CircularProgressIndicator(
                              strokeWidth: 2,
                            )
                          : MaterialButton(
                              onPressed: () {
                                if (formKey.currentState.validate()) {
                                  formKey.currentState.save();
                                  _login();
                                }
                              },
                              color: Theme.of(context).accentColor,
                              height: 50.0,
                              minWidth: 300.0,
                              child: Text(
                                "LOGIN",
                                style: _sizeTextWhite,
                              ),
                            ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
