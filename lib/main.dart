import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tread_app/Widgets/PagesRoutes.dart';
import 'Repositories/CommentRepository/CommentsProvider.dart';
import 'Repositories/PostRepository/PostProvider.dart';
import 'Repositories/SortRepository/SortProvider.dart';
import 'Repositories/UserRepository/UserProvider.dart';
import 'Widgets/Login/Login.dart';
import 'Widgets/MainPage/MainPage.dart';
import 'Widgets/Profile/Profile.dart';

void main() => runApp(
  MultiProvider(
    providers: [
      ChangeNotifierProvider<UserProvider>(create: (context) => UserProvider()),
      ChangeNotifierProvider<PostProvider>(create: (context) => PostProvider()),
      ChangeNotifierProvider<CommentsProvider>(create: (context) => CommentsProvider()),
      ChangeNotifierProvider<SortProvider>(create: (context) => SortProvider()),
    ],
    child: MaterialApp(
        initialRoute: "/login",
        routes: {
          PagesRoutes.login: (context) => Login(),
          PagesRoutes.main: (context) => MainPage(),
          PagesRoutes.profile: (context) => UserProfile(),
        }
    ),
  ),
);

